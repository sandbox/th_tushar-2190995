
Description
-----------
The module facilitates the function to add the responsive carousels to yor site.
For more information about jCarousel responsive, visit the official project:
http://sorgalla.com/jcarousel/examples/responsive/


Installation
------------
1) Place this module directory in your modules folder (this will usually be
   "sites/all/modules/").

2) Enable the module within your Drupal site at Administer -> Site Building ->
   Modules (admin/build/modules).

Usage
-----
This module can be commonly used with the Views module to display the
listings of images or other content into a carousel.

1) Install the Views module (http://drupal.org/project/views) on your Drupal
   site if you have not already.

2) Add a new view at Administration -> Structure -> Views
(admin/structure/views).

3) Change the "Display format" of the view to "jCarousel Responsive". Disable
   the "Use pager" option, which cannot be used with the jCarousel responsive
   style.
   Click the "Continue & Edit" button to configure the rest of the View.

4) Add the items you would like to include in the rotator under the "Fields"
   section, and build out the rest of the view as you would normally. Note that
   the preview of the carousel within Views probably will not appear correctly
   because the necessary JavaScript and CSS is not loaded in the Views
   interface. Save your view and visit a page URL containing the view to see
   how it appears.

API Usage
---------
The jcarousel_add function allows you to not only add the required jCarousel
JavaScript, and apply the jCarousel behavior to the elements on the page. The
arguments are as follows:

	Just return the below HTML to the rendering template and invoke the 
	jcarousel_responsive() method to include the required Javascript and CSS
	files.
	
	<div class="jcarousel-wrapper">
    <div class="jcarousel">
      <ul>
		    <li><img src="'.drupal_get_path('module', 'jcarousel_responsive')
		        .'/img/img1.jpg" alt="" /></li>
		    <li><img src="'.drupal_get_path('module', 'jcarousel_responsive')
		        .'/img/img2.jpg" alt="" /></li>
		    <li><img src="'.drupal_get_path('module', 'jcarousel_responsive')
		        .'/img/img3.jpg alt="" /></li>
		    <li><img src="'.drupal_get_path('module', 'jcarousel_responsive')
		        .'/img/img4.jpg alt="" /></li>
		    <li><img src="'.drupal_get_path('module', 'jcarousel_responsive')
		        .'/img/img5.jpg alt="" /></li>
		    <li><img src="'.drupal_get_path('module', 'jcarousel_responsive')
		        .'/img/img6.jpg alt="" /></li>';
      </ul>
    </div>
    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
	  <a href="#" class="jcarousel-control-next">&rsaquo;</a>
 		<p class="jcarousel-pagination"></p>
	</div>
	  
  <?php jcarousel_responsive(); ?>
  
  If you are using a custom module, and want to use a theme function,
  you can do
  it as below,
  
  $items = array(
          '<img src="' . $base_url . '/' . drupal_get_path('module',
              'jcarousel_responsive') . '/img/img1.jpg" alt="" />',
          '<img src="'.$base_url.'/'.drupal_get_path('module',
              'jcarousel_responsive') . '/img/img2.jpg" alt="" />',
          '<img src="' . $base_url . '/' . drupal_get_path('module',
              'jcarousel_responsive') . '/img/img3.jpg" alt="" />',
          '<img src="' . $base_url . '/' . drupal_get_path('module',
              'jcarousel_responsive') . '/img/img4.jpg" alt="" />',
          '<img src="' . $base_url . '/' . drupal_get_path('module',
              'jcarousel_responsive') . '/img/img5.jpg" alt="" />',
          '<img src="' . $base_url . '/' . drupal_get_path('module',
              'jcarousel_responsive') . '/img/img6.jpg" alt="" />',
        );
  $output .= theme('jcarousel_responsive', array('items' => $items));
  return $output;
  
  As simple as that!!!!

You can also visit "admin/help#jcarousel_responsive" of your site for help 

Note: Disable the Overlay Module when using Views, if not, may disturb the
Views User Interface. Check for the proper carousel on the page where the
View's data will be displayed.
