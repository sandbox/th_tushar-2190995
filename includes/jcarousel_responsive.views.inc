<?php

/**
 * @file
 * Integration of jCarousel Responsive module with Views Module.
 */

/**
 * Implements hook_views_plugins().
 */
function jcarousel_responsive_views_plugins() {
  $plugins['style']['jcarousel_responsive'] = array(
    'title' => t('jCarousel Responsive'),
    'help' => t('Display rows in a carousel via jCarousel.'),
    'handler' => 'jcarousel_responsive_style_plugin',
    'path' => drupal_get_path('module', 'jcarousel_responsive') . '/includes',
    'theme' => 'jcarousel_responsive_view',
    'theme path' => drupal_get_path('module', 'jcarousel_responsive') .
    '/includes',
    'uses row plugin' => TRUE,
    'uses options' => TRUE,
    'uses grouping' => FALSE,
    'type' => 'normal',
  );
  return $plugins;
}

/**
 * Preprocess function for jcarousel-responsive-view.tpl.php.
 */
function template_preprocess_jcarousel_responsive_view(&$variables) {
  // Include the required JavaScript and CSS library for the carousel.
  drupal_add_library('jcarousel_responsive', 'jcarousel_responsive');
}
