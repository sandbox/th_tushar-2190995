<?php

/**
 * @file
 * Contains the responsive jCarousel style plugin.
 */

/**
 * The style plugin for the responsive jcarousel.
 *
 * @ingroup views_style_plugins
 */
class jcarousel_responsive_style_plugin extends views_plugin_style {
  /**
   * The function to define the settings form for a plugin.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['display_note'] = array(
      '#type' => 'item',
      '#title' => t('Note:'),
      '#markup' => t('The module is available with default enabled
      configurations provided by the javascript plugin.'),
    );
  }
}
