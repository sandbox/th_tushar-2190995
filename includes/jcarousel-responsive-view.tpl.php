<?php

/**
 * @file
 * The items returned from view module are rendered using this template.
 */
?>
<div class="jcarousel-wrapper">
  <div class="jcarousel">
    <ul>
      <?php foreach ($rows as $row): ?>
        <li><?php print $row; ?></li>
      <?php endforeach; ?>
    </ul>
	</div>
  <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
  <a href="#" class="jcarousel-control-next">&rsaquo;</a>
  <p class="jcarousel-pagination"></p>
</div>
